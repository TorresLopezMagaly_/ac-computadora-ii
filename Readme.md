# Arquitectura de computadoras-ISB
## Computadoras electrónicas-Mapa conceptual

```plantuml
@startmindmap
* Computadoras electrónicas


** Harvard Mark I (IBM – 1944)
*** Construida por IBM durante la segunda guerra mundial.
*** Características: -765000 componentes \n80 kilómetros de cable un eje de 15 metros \nMotor de 5 caballos de fuerza
*** Usos: Hacer simulaciones y cálculos para el proyecto Manhattan
**** El proyecto Manhattan es el que desarrollo la bomba atómica
*** Relé\n Es un interruptor mecánico controlado eléctricamente 
**** Relé en la Harvard Mark I: \nTenía aproximadamente 3500 relés.\nPor estadística fallaba un relé al día.
**** Relé en 1940: \nTenía la capacidad de cambiar de estados unas 50 veces por segundo 

**** Compuesto por: Una bobina (alambre de cobre aislado y enrollado)\nUn brazo de hierro\nDos contactos 
**** Funcionamiento: \n Cuando una corriente eléctrica atraviesa la bobina se genera un campo magnético este atrae al \nbrazo mecánico y ya que el brazo mecánico está conectado a los contactos los une para así cerrar el circuito.

*** Alcances: \n3 sumas o restas por segundo \nMultiplicación en 6 segundos \nDivisión en 15 segundos \nOperaciones complejas, en minutos 
*** Relé en la Harvard Mark I:\nTenía aproximadamente 3500 relés. \nPor estadística fallaba un relé al día.
*** Problemas de la Harvard Mark I: \nEn septiembre de 1947, los operadores de la maquina descubrieron un problema en ella y lo estaba ocasionando una \npolilla muerta de la máquina, atraída por el calor de la maquia como muchos otros insectos.
**** Grace Hopper - Científica de computación \nFue quien menciono que cuando algo salía mal con una computadora es porque tenía bichos, de aquí salió el termino bug.

**  Válvula termoiónica 
*** John  Ambrose Fleming físico ingles   desarrollo la válvula termoiónica en 1904 
*** Componentes: \nUn filamento \n 2 electrodos situados dentro de un bulbo de cristal sellado 
*** Funcionamiento: cuando el filamento se enciende y se calienta permite el flujo de electrones desde el ánodo \nhacia el cátodo, este proceso es llamado emisión termoiónica, que permite el flujo de la corriente en una sola dirección que hoy es conocido como diodo.
**** Lee de Forest 1906 Inventor americano, encontró el tercer electrodo, el electrodo de control.
**** Electrodo de control: \n situado entre el ánodo y cátodo del diseño de Fleming 
**** Funcionamiento: \n Al aplicar una carga negativa o positiva en el electrodo de control permite el flujo o detiene la corriente respectivamente 
*** Ventajas: \nMisma funcionalidad de los relés pero sin las partes móviles \n Mayor velocidad \n Menos daño por el uso 
*** Desventajas: \nLos tubos eran frágiles y se podían quemar como los focos o bulbos de luz. \Eran costosos. \ En 1940 eran más accesibles pero para personas dentro del gobierno

** Colosus Mark I 
*** Diseñada por el ingeniero Tommy Flowers, finalizada en diciembre de 1943
*** Conocida como la primera computadora electrónica programable 
*** Primera versión: \n Tenia 1,600 tubos de vacío \n Se construyeron 10 colosus para ayudar en la decodificación 
*** Configuración: \n Mediante la conexión de cientos de cables en un tablero 
*** Ubicación: \n Fue instalada en el parque Bletchley en Reino Unido 
*** Uso: \n Ayudaba a decodificar las comunicaciones nazis
**** The Bombe: Fue diseñada 2 años antes por Alan Turing  para descifrar el código enigma nazi


** Eniac 
*** Construida en 1946 en la universidad de Pensilvania diseñada por John Mauchly y J. Presper Eckert
*** Siglas de Calculadora, Integradora, numérica y electrónica 
*** Primera computadora electrónica programable de propósito general
*** Podía realizar 5 mil sumas y restas de diez dígitos por segundo \nOpero durante 10 años
*** Desventaja: Debido a las constantes fallas de los tubos de vacío Eniac solo era operacional durante la mitad del día antes de dañarse

** Transistor 
*** Los científicos de laboratorio Bell  John Bardeen, William Shockley y Walter Brattain en 1947 crearon el transistor
**** Con su invención se inició una nueva era de la computación 
*** Con su invención se inició una nueva era de la computación 
*** Un transistor es un interruptor eléctrico
*** Construcción: \nEsta construido con 3 capas de los tipos de silicio en forma de sándwich, cada capa representa un conector llamados colector emisor y base 
*** Compuestos: \nSilicio, no permite el paso de corriente eléctrica. Por la acción del  dopaje se le pueden agregar otros componentes al silicio y de eso depende la corriente eléctrica, entonces dependiendo de lo anterior el silicio puede tener exceso defecto de electrones.
*** Funcionamiento: \nAl haber flujo de corriente entre la base y el emisor, este permite que exista un flujo de corriente entre el emisor y el colector, funcionando así como interruptor eléctrico.
*** Ventajas: \nTransistores resistentes \n Se podía adaptar su tamaño, no como los relés o tubos de vacío 

** IBM 608
*** Lanzada en 1957
*** Primera computadora disponible comercialmente basada enteramente en transistores 
*** Realizaba cerca de  4500 sumas \n80 divisiones o multiplicaciones por segundo 
*** IBM comenzó a usar transistores en todos sus productos, acercando de esta forma las computadoras a las oficinas y a los hogares 


@endmindmap
```

##Arquitectura Von Neumann y Arquitectura Harvard - Mapa conceptual

```plantuml
@startmindmap

* Arquitectura Von Neumann y Arquitectura Harvard
** Ley de Moore
*** Establece que la velocidad del procesador o el poder de procesamiento total de las computadoras se duplica cada 12 meses.
*** Electrónica
**** El número de transistores por chip se duplica cada año. El costo del chip permanece sin cambios.\nCada 18 meses la potencia de cálculo sin modificar costo se duplica.
*** Performance
**** Se incrementa la velocidad del procesador y de capacidad de memoria, siempre corre por detrás la velocidad de memoria por la del procesador.
*** Funcionamiento de una computadora
**** Antes el flujo del funcionamiento con sistemas cableados era datos de entrada, luego una secuencia de funciones aritméticas y lógicas y así obtener los resultados. La programación era a través de hardware.
**** Ahora, se inicia de igual forma por los datos de entrada, de ahí se hace la programación con software, indicando las instrucciones al procesador implementadas en compuertas lógicas que decodificara y nos dará los resultados.

** Arquitectura Von Newman
*** Principales componentes
**** CPU: Se compone de registros: control de programa, instrucción, dirección de memoria, buffer \nde memoria, dirección de e/s, buffer de e/s. Memoria principal
**** Módulo de entrada/salida: Componentes hardware que están conectados a la computadora como una pantalla, disco duro, entradas con las que se pueden conectar más componentes.
**** Bus del sistema: Con el que se intercambia la información entre la cpu, memoria y los módulos e/s
***** Bus de control: Unidireccional \nLo usa la cpu para indicar que operación quiere realizar a la memoria central como lectura o escritura.
***** Bus de direcciones: Unidireccional \nLo usa la cpu para indicar las direcciones de dónde se localiza el espacio de memoria dónde se realizará la lectura/escritura.
***** Bus de datos e instrucciones: Bidireccional \nLo usa la cpu para pasar los datos cuando se realiza una escritura, y la memoria envía los datos a la cpu cuando se realiza una lectura.
**** Tiene una ejecucion de manera secuencial
*** Memoria de escritura-lectura, aquí se almacenan datos y programas
**** A la memoria se accede con sus posiciones pero no importa el tipo.

** Arquitectura Harvard
*** Modelo
**** Arquitectura de computadoras que utilizaban dispositivos de almacenamiento fisicamente separados \npara las instrucciones y para los datos
*** Contiene 
**** Unidad central de procesamiento (CPU)
***** contiene una unidad de control, unidad aritmetica logica y registros.
**** Memoria principal
***** Puede almacenar tanto instrucciones como datos
**** Sistema de entrada / salida
**** Procesador
***** Unidad de control (UC)
****** La UC lee la isntruccion de la memoria de instrucciones, \ngenera las señales de control para obtener los operandos de memoria de datos y luego  ejecuta la instruccion medinate la ALU almacenando el \nresultado en la memoria de datos
***** Unidad aritmética y logica (ALU)
*** Memorias 
**** Memoria de instrucciones 
***** En ella se almacenan las instrucciones del programa que debe ejecutar el microcontrolador 
**** Memoria de datos
***** Se almacenan los datos utilizados por los programas.
**** Fabrica memorias mucho mas rapido, \ntiene un precio muy alto.
***** Solucion: Proporcionar una pequeña cantidad de memoria muy rapida conocida con el nombre de CACHE
***** Mientras los datos que necesite elprocesador esten en el cache, el rendimiento sera mucho mayor
**** Solución Harvard
***** Las instrucciones y los datos se almacenan en caches separados para mejorar el rendimiento.
***** Tienen el inconveniente de tener que dividir la cantidad de cahe entre los dos, \npor lo que funciona mejor solo cuando la frecuencia de lectura de instrucciones y de datos ess aproximadamente la misma.
*** Uso
**** Esta arquitectura suele usarse en PICs o microcontroladores, usados habitualmente en productos de proposito especifico \ncomo electrodomesticos, procesamiento de audio, video, etc..

















@endmindmap
```

## Basura Electrónica - Mapa conceptual

```plantuml
@startmindmap

* Basura electrónica 

** Causas
*** La evolucion de la tecnologia y la sobrepoblacion han llevado a que cada vez el consumo \de aparatos electricos y electronicos se desborde. El problema mas obvio e importante ahora es que no solo se consume en grandes cantudades para faciltar la vida si no que \nno se le da un debido tratamiento a los residuos y eso afecta al medio ambiente y muchas veces tambien a los datos que consideramos perdidos, exponiendo asi nuestra salud y seguridad.
** RAEE
*** Residuos o aparatos eléctricos y electrónicos que cuando dejan de funcionar se convierten en un RAEE 
**** Por su condición ameritan un manejo especial 
*** Complejo de metales, plásticos y otros compuestos que deben ser debidamente tratados
*** Planta de reciclaje de Perú Green recycling. Aquí se le da tratamiento a los aparatos eléctrico, electrónicos.
**** Proceso: Recolección \nSelección según su categoría \nSe desarman para recuperar materiales primarios para así reintroducirlos a una nueva cadena productiva
***** Las placas se clasifican por un especialista
**** Para al final ser usados para nuevos productos o exportados a otros continentes 
**** Residuos peligrosos: lo que debe ser depositado en un relleno
***** Relleno de seguridad: Posas protegidas con membranas especiales capaces de evitar el derrame de estos residuos peligrosos
** Reciclaje de desechos electronicos - Mexico
*** En México anualmente se desechan entre 150 y 180 mil toneladas de basura electronica, entre equipos de computo, celulares, etc, todos estos sin regulacion ni control 
*** Recepcion del materiar electrico 
*** Desarmarlo y clasificarlo de acuerdo a sus componentes
*** Segregacion y limpieza de todas las piezas a continuacion se almacena con su correspondiente clasificacion
*** Finalmente se exporta para que ma matrix realice el ultimo trabajo de fundicion y recuperacion de los metales
** Recclaje de desechos electronicos - Costa Rica
*** Empresa servicios ecologigos
**** Es una empresa de punto intermedio entre el generador y procesador final
*** Proceso
**** Se desarman los desechos, y dejar limpia cada parte de los componentes de un aparato
**** Separacion, por plastico, por cobre, aluminio y desechos peligroso
**** Otro tipo de clasificacion: Clasifican las tarjetas madre, las pesan y desarman
***** Toman el cobre , bronce, tornillos, tarjetas pequeñas y basura, lo ue pasa despus es que se almacena y se espera a que algun cliente del exterior lo compre
*** La empresa es una fuente de empleo y de progreso ecologico para el pais

** Laptos funcionales
*** Empresas tiran aparatos electronicos porque estos ya les parecen obsoletos o porque compraron equipo nuevo
*** Estas empresas no consideran el daño que hacen al ambiente 
*** La información no deberia perderse ya que se hace un trato entre la empresa y la persona a la cual se le entregan los aparatos \npara su reciclaje, se firma un certificado de destruccion de discos duros para asi proteger la información de la empresa.

** Cementerio de computadoras 
*** Es dificil imaginar que la mayoria de aparatos que se encuentran en la basura aun sirven.
*** Varias personas se dedican a reciclar estos aparatos, algunos que aun sirven los ponen a la venta y muchos otros que no solo recuperan piezas para venderlas.
*** En estos lugares se encuentra desde un aparato con mas de 20 años hasta uno de pocos dias, es increible la rapidez con la que estos aparatos son desechados. 



























@endmindmap
```
